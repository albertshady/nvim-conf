source $HOME/.config/nvim/settings.vim
source $HOME/.config/nvim/mappings.vim
source $HOME/.config/nvim/buffers.vim
source $HOME/.config/nvim/triggers.vim
source $HOME/.config/nvim/plugins.vim
source $HOME/.config/nvim/lua/lua.vim
source $HOME/.config/nvim/lua/lsp.vim
source $HOME/.config/nvim/signify.vim

" source $HOME/.config/nvim/themes/onedark.vim
" source $HOME/.config/nvim/themes/gruvbox.vim
source $HOME/.config/nvim/themes/dracula.vim

source $HOME/.config/nvim/plugins/init.vim
