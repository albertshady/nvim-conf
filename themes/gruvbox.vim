" set background=light " or light if you want light mode
colorscheme gruvbox

if (has('termguicolors'))
    set termguicolors
endif
