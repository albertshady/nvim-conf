" when startup
autocmd vimenter * if !argc() | NERDTree | endif    " launch NERDTree if no args passed

" before writing
autocmd BufWritePre * :%s/\s\+$//e                  " trim trailing whitespaces
autocmd BufWritePre * :%s/\($\n\s*\)\+\%$//e        " trim final newlines

" python autopair special strings
autocmd FileType python let b:AutoPairs = AutoPairsDefine({"f'" : "'", "r'" : "'", "b'" : "'"})

" reset cursor
autocmd VimLeave * set guicursor=a:hor90
