source $HOME/.config/nvim/plugins/sources.vim

source $HOME/.config/nvim/plugins/rainbow.vim
source $HOME/.config/nvim/plugins/lspconfig.vim
source $HOME/.config/nvim/plugins/treesitter.vim
source $HOME/.config/nvim/plugins/telescope.vim
source $HOME/.config/nvim/plugins/dashboard.vim
